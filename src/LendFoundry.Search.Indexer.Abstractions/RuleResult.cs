﻿using System.Collections.Generic;

namespace LendFoundry.Search.Indexer
{
    public class RuleResult
    {
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
    }
}
