﻿namespace LendFoundry.Search.Indexer
{
    public interface ISearchIndexerListener
    {
        void Start();
    }
}