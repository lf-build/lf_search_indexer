﻿using System;

namespace LendFoundry.Search.Indexer
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "search-indexer";
    }
}