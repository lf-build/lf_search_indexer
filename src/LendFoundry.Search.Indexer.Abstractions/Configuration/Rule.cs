﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendFoundry.Search.Indexer.Configuration
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
