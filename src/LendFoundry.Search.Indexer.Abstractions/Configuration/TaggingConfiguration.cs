﻿using System.Collections.Generic;

namespace LendFoundry.Search.Indexer.Configuration
{
    public class TaggingConfiguration
    {
        public string Name { get; set; }
        public string IndexKey { get; set; }
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
        public List<Rule> Rules { get; set; }
        public string EntityType { get; set; }
    }
}
