﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Search.Indexer.Configuration
{
    public sealed class SearchIndexerConfiguration: IDependencyConfiguration
    {
        public Dictionary<string, List<EventConfiguration>> IndexConfiguration { get; set; }
        public Dictionary<string, List<TaggingConfiguration>> TaggingConfiguration { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}