﻿namespace LendFoundry.Search.Indexer.Configuration
{
    public class EventConfiguration
    {
        public string Name { get; set; }
        public string IndexKey { get; set; }
        public string SectionName { get; set; }
        public string RuleName { get; set; }
        public string EntityType { get; set; }
    }
}