﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Listener;
using LendFoundry.Search.Indexer.Configuration;
using LendFoundry.Search.Abstractions;

namespace LendFoundry.Search.Indexer
{
    public class SearchIndexerListener : ListenerBase, ISearchIndexerListener
    {
        public SearchIndexerListener
        (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            IConfigurationServiceFactory<SearchIndexerConfiguration> configurationSearchIndexerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IDecisionEngineClientFactory decisionEngineFactory,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory, ILookupClientFactory lookupServiceFactory, ISearchProxyFactory searchProxyFactory) : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(configurationSearchIndexerFactory), configurationSearchIndexerFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);
            EnsureParameter(nameof(searchProxyFactory), searchProxyFactory);

            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            ConfigurationSearchIndexerFactory = configurationSearchIndexerFactory;
            TenantServiceFactory = tenantServiceFactory;
            DecisionEngineFactory = decisionEngineFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            LookupServiceFactory = lookupServiceFactory;
            SearchProxyFactory = searchProxyFactory;

        }

        #region Variables

        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory<SearchIndexerConfiguration> ConfigurationSearchIndexerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ILookupClientFactory LookupServiceFactory { get; }
        private ISearchProxyFactory SearchProxyFactory { get; }

        #endregion

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var eventHub = EventHubFactory.Create(reader);

            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var configuration = ConfigurationSearchIndexerFactory.Create(reader).Get();
            var decisionEngine = DecisionEngineFactory.Create(reader);
            var lookupService = LookupServiceFactory.Create(reader);
            var searchService = SearchProxyFactory.Create(reader);

            if (configuration != null && configuration.IndexConfiguration != null)
            {
                var lookupEntries = lookupService.GetLookupEntries("entityTypes");
                var uniqueEvents = new List<string>();
                foreach (var entity in configuration.IndexConfiguration)
                {
                    var entityType = entity.Key;
                    var configKeyDefinition = configuration.IndexConfiguration[entityType].ToList();
                    var tagKeyDefinition = configuration.TaggingConfiguration[entityType].ToList();
                    uniqueEvents.AddRange(configKeyDefinition.Select(x => x.Name).Distinct().ToList());
                    uniqueEvents.AddRange(tagKeyDefinition.Select(x => x.Name).Distinct().ToList());

                    uniqueEvents.Distinct().ToList().ForEach(eventName =>
                    {
                        eventHub.On(eventName, ParseResponse(configuration, decisionEngine, logger, searchService, lookupEntries));
                        logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                    });
                }
                return uniqueEvents;
            }
            else
            {
                logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                return null;
            }

        }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<SearchIndexerConfiguration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                return null;
            }
            else
            {
                return configuration.IndexConfiguration.SelectMany(ent => ent.Value.Select(c => c.Name)).Distinct().ToList();
            }
        }

        private static Action<EventInfo> ParseResponse(SearchIndexerConfiguration configuration, IDecisionEngineService decisionEngine, ILogger logger, ISearchProxy searchProxy, Dictionary<string, string> lookupEntries)
        {
            return @event =>
            {
                var indexMatches = FindMatchingIndexConfigurations(configuration, @event);
                foreach (var match in indexMatches)
                {
                    var eventConfigurationEntityType = match.eventConfigurationEntityType;
                    if (!string.IsNullOrWhiteSpace(eventConfigurationEntityType))
                    {
                        if (string.Equals(eventConfigurationEntityType, match.entityType, StringComparison.InvariantCultureIgnoreCase))
                        {
                            ParseIndex(match, decisionEngine, logger, searchProxy, @event, lookupEntries);
                        }
                    }
                    else
                    {
                        ParseIndex(match, decisionEngine, logger, searchProxy, @event, lookupEntries);
                    }

                }
                var tagMatches = FindMatchingTagConfigurations(configuration, @event);
                foreach (var tag in tagMatches)
                {
                    var tagConfigurationEntityType = tag.tagConfigurationEntityType;
                    if (!string.IsNullOrWhiteSpace(tagConfigurationEntityType))
                    {
                        if (string.Equals(tagConfigurationEntityType, tag.entityType, StringComparison.InvariantCultureIgnoreCase))
                        {
                            ParseTags(tag, decisionEngine, logger, searchProxy, @event, lookupEntries);
                        }
                    }
                    else
                    {
                        ParseTags(tag, decisionEngine, logger, searchProxy, @event, lookupEntries);
                    }
                }
            };
        }

        private static void ParseIndex(dynamic match, IDecisionEngineService decisionEngine, ILogger logger, ISearchProxy searchProxy, EventInfo @event, Dictionary<string, string> lookupEntries)
        {
            logger.Info($"Processing {@event.Name} with Id - {@event.Id} for {@event.TenantId}");

            try
            {
                var parameter = new { match.entityType, match.entityId, @event };
                var result = decisionEngine.Execute(match.ruleName, parameter);
                if (result != null)
                {
                    Dictionary<string, dynamic> pairs = new Dictionary<string, dynamic>();
                    pairs.Add(match.sectionName, result);
                    searchProxy.PatchAsync(match.entityType, match.entityId, pairs).Wait();
                }
                else
                {
                    logger.Warn($"No Response returned from rule {match.ruleName} for {@event.TenantId}", new { @event, match });
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error parsing event", ex, new { @event, match });
            }
        }

        private static void ParseTags(dynamic match, IDecisionEngineService decisionEngine, ILogger logger, ISearchProxy searchProxy, EventInfo @event, Dictionary<string, string> lookupEntries)
        {
            logger.Info($"Processing {@event.Name} for tags with Id - {@event.Id} for {@event.TenantId}");

            try
            {
                List<string> addTags = new List<string>();
                List<string> removeTags = new List<string>();
                if (match.applyTags != null && match.applyTags.Count > 0)
                    addTags.AddRange(match.applyTags);
                if (match.removeTags != null && match.removeTags.Count > 0)
                    removeTags.AddRange(match.removeTags);
                foreach (var rule in match.rules)
                {
                    var parameter = new { match.entityType, match.entityId, @event };
                    RuleResult result = decisionEngine.Execute<dynamic, RuleResult>(rule.Name, parameter);
                    if (result != null)
                    {
                        if (result.ApplyTags != null && result.ApplyTags.Any())
                            addTags.AddRange(result.ApplyTags);
                        if (result.RemoveTags != null && result.RemoveTags.Any())
                            removeTags.AddRange(result.RemoveTags);
                    }
                    else
                    {
                        logger.Warn($"No Response returned from rule {match.ruleName} for {@event.TenantId}", new { @event, match });
                    }
                }
                if (addTags.Any())
                    searchProxy.AddTagsAsync(match.entityType, match.entityId, addTags.ToArray()).Wait();
                if (removeTags.Any())
                    searchProxy.RemoveTagsAsync(match.entityType, match.entityId, removeTags.ToArray()).Wait();

                logger.Info($"Successfully processed {@event.Name} for tags with Id - {@event.Id} for {@event.TenantId}");
            }
            catch (Exception ex)
            {
                logger.Error("Error parsing event", ex, new { @event, match });
            }
        }

        private static IEnumerable<dynamic> FindMatchingIndexConfigurations(SearchIndexerConfiguration configuration, EventInfo @event)
        {
            return from indexConfiguration in configuration.IndexConfiguration
                   from eventConfiguration in indexConfiguration.Value
                   where string.Equals(eventConfiguration.Name, @event.Name, StringComparison.InvariantCultureIgnoreCase)
                   select new
                   {
                       entityType = indexConfiguration.Key,
                       entityId = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.IndexKey.Replace("{", "").Replace("}", ""), @event)),
                       ruleName = eventConfiguration.RuleName,
                       name = eventConfiguration.Name,
                       sectionName = eventConfiguration.SectionName,
                       eventConfigurationEntityType = !string.IsNullOrWhiteSpace(eventConfiguration.EntityType) ? Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.EntityType.Replace("{", "").Replace("}", ""), @event)) : string.Empty
                   };
        }

        private static IEnumerable<dynamic> FindMatchingTagConfigurations(SearchIndexerConfiguration configuration, EventInfo @event)
        {
            return from indexConfiguration in configuration.TaggingConfiguration
                   from tagConfiguration in indexConfiguration.Value
                   where string.Equals(tagConfiguration.Name, @event.Name, StringComparison.InvariantCultureIgnoreCase)
                   select new
                   {
                       entityType = indexConfiguration.Key,
                       entityId = Convert.ToString(InterpolateExtentions.GetPropertyValue(tagConfiguration.IndexKey.Replace("{", "").Replace("}", ""), @event)),
                       rules = tagConfiguration.Rules,
                       name = tagConfiguration.Name,
                       applyTags = tagConfiguration.ApplyTags,
                       removeTags = tagConfiguration.RemoveTags,
                       tagConfigurationEntityType = !string.IsNullOrWhiteSpace(tagConfiguration.EntityType) ? Convert.ToString(InterpolateExtentions.GetPropertyValue(tagConfiguration.EntityType.Replace("{", "").Replace("}", ""), @event)) : string.Empty
                   };
        }

        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
    }
}
