﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;
using Microsoft.AspNetCore.Http;
using LendFoundry.Foundation.Services;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Search.Indexer.Configuration;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Search.Client;

namespace LendFoundry.Search.Indexer.App
{
    /// <summary>
    /// Entrypoint
    /// </summary>
    public class Program : DependencyInjection
    {
        /// <summary>
        /// Entrypoint
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Program p = new Program();
            p.Provider.GetService<ISearchIndexerListener>().Start();
        }

        /// <summary>
        /// Configure the services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);

            services.AddDecisionEngine();
            services.AddLookupService();
            services.AddSearch();

            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddConfigurationService<SearchIndexerConfiguration>(Settings.ServiceName);
            services.AddTransient<ISearchIndexerListener, SearchIndexerListener>();
            services.AddDependencyServiceUriResolver<SearchIndexerConfiguration>(Settings.ServiceName);

            return services;
        }
    }
}